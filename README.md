# Peerplays Ecosystem Diagrams

Here you'll find diagrams of the various components of Peerplays. Below is a non-exhaustive list of Peerplays components to be mapped. Each diagram will have two files: an editable XML file (for diagrams.net), and an exported PNG file (for easy viewing).

## Level 0

- Peerplays Ecosystem

## Level 1

- Liquidity Pools (LPs)
- Gamified Proof of Stake (2.0)
- Automated Market Maker (AMM)
- Peerplays Decentralized Exchange (Dex)
- NFT Marketplace
- Sidechain Operator Nodes (SONs)
- Witness Nodes
- Decentralized Applications (DApps)
- Peerplays Communities
- Blockchain Governance

## Level 2

- Non-Fungible Tokens (NFTs)
- Community Asset Tokens (CATs)

## NEX

- NEX-FS01 Dashboard
- NEX-FS05 Market Page
- NEX-FS06 Profile Page
- NEX-FS07 Wallet Functions
- NEX-FS08 App Settings
- NEX-FS09 Blockchain Page
- NEX-FS10 GPOS Page

## Miscellaneous

- Speak Network
- Proof of Pulse Consensus
